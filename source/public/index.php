<?php
/**
 * index.php
 *
 * This is the default bootstrapper for Simplicity.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @package core
 *
 */

require '../lib/simplicity/simplicity.php';

Simplicity::getInstance()->setOptions(array(
	'error_handling' => true
))->init()->run();
