<?php
require_once dirname(__FILE__).'/core/pluggable/pluggable.php';

/**
 * Simplicity
 *
 * The main public interface of the Simplicity Framework. This class bootstraps your application and manages and executes the module chain.
 *
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @see smp_Tapped
 * @smp_core
 * 
 * @smp_children smp_Module dynamic
 * @smp_children smp_ModuleStatic static
 */
class Simplicity extends smp_Pluggable {

	static private $_instance;

	private $_options = array(
		'tapped_cache' => 86400,
		'config_backend' => 'Array',
		'config_backend_options' => array('cache_timeout' => 3600),
		'error_handling' => true,
		'base_url' => '',
		'environment' => false,
		'environments' => array(),
		'config_path' => '{APP}config',
		'lib_path' => null,
		'app_path' => null,
		'tmp_path' => null
	);

	/**
	 * Config driver instance
	 *
	 * @var smp_ConfigDriver
	 */
	private $_config;

	private $_lib;
	private $_root;
	private $_app;
	private $_tmp;

	private $_modules = array();

	private $_last_module = false;

	private $_base = array();
	private $_chains = array();
	private $_chain = array();

	private $_alias = array();

	/**
	 * Returns the single Simplicity instance.
	 *
	 * @return Simplicity
	 */
	static public function getInstance()
	{
		if (!(self::$_instance instanceof Simplicity))
		{
			self::$_instance = new Simplicity();
		}
		return self::$_instance;
	}

	private function __clone() {}
	private function __construct() {}

	/**
	 * Sets any runtime options for the Simplicity framework.
	 *
	 * @param $options array An associative array of options to configure Simplicity.
	 *
	 * @return Simplicity
	 */
	public function setOptions(array $options = array())
	{
		$this->_options = $options + $this->_options;
		return $this;
	}

	public function initEnv()
	{
		$this->initEnvironment();
		$this->initArchive();
		$this->initTapped();
		
		smp_Enviroment::init();
		
		smp_Request::init($this->_options['base_url']);
		
		return $this;
	}
	
	/**
	 * Initialises the Simplicity framework
	 *
	 * @return Simplicity
	 */
	public function init()
	{
		if (!defined('APP')) $this->initEnv();
				
		$this->createExtensionPoint('smp_Module','dynamic');
		$this->registerPluginPrefix('smp_','dynamic');
				
		$this->createExtensionPoint('smp_ModuleStatic','static');
		$this->registerPluginPrefix('smp_','static');
				
		$this->initConfig();
		
		if ($this->_options['error_handling']) {
			$this->getModule('Error')->exec(array());
		}
		
		return $this;
	}

	/**
	 * run
	 * 
	 * Executes the module chain(s) and dies.
	 *
	 */
	public function run()
	{
		$chains = array();

		$this->_alias = $this->_config->get('modules.alias');
		$this->_chains = $this->_config->get('modules.chains');
		
		$chains = array(
			'_static' => array(
				'chain' => array('Static'),
				'match' => '/_static/*',
				'nobase' => true
			)
		);

		$this->_chains = is_array($this->_chains) ? $this->_chains : array();
		$this->_chains = array_merge($chains,$this->_chains);
			
		$this->_base = isset($this->_chains['_base']['chain']) ? $this->_chains['_base']['chain'] : array();
			
		unset($this->_chains['_base']);
		
		$match = false;
		foreach ($this->_chains as $name => $chain)
		{
			if (!isset($chain['match']))
			{
				$this->runChain($name);
				break;
			}
			elseif (isset($chain['match']) && smp_Request::getInstance()->uriMatch($chain['match']))
			{
				$base = str_replace('//','/',$this->_options['base_url'].$chain['match']);
				smp_Request::getInstance()->setBaseUri($base);
				$this->runChain($name);
				break;
			}
		}
	}

	public function __destruct()
	{
		if (smp_Enviroment::getInstance()->getSapi() != 'cli') {
			smp_Response::getInstance()->sendStatus();
			smp_Response::getInstance()->sendHeaders();
		}
		
		smp_Response::getInstance()->sendContent();
	}
	
	/**
	 * runChain
	 * 
	 * Run the specified $chain.
	 * 
	 * @param $chain string
	 */
	public function runChain($chain,$input=array())
	{
		if (!isset($this->_chains[$chain])) throw new smp_Exception("Chain {$chain} does not exist.");
		$chain = $this->_chains[$chain];

		if (isset($chain['chain']) && is_array($chain['chain'])) {
			$this->_chain = (isset($chain['chain']) && is_array($chain['chain'])) ? $chain['chain'] : array();
		}

		if (!isset($chain['nobase']) || !$chain['nobase']) {
			$this->_chain = array_merge($this->_base,$this->_chain);
		}

		$output = $input;

		while($module = array_shift($this->_chain)) {
			$output = $this->runModule($module,$output);
		}
		
		return $output;
	}

	/**
	 * runModule
	 * 
	 * Executed the specified module $alias and an array of arguments. Module identifiers take the format of alias or alias:method.
	 *
	 * @param $module
	 * @param $args
	 * @return array
	 */
	public function runModule($alias,$args=array())
	{		
		$alias = explode(':',$alias,2);
		$method = isset($alias[1]) ? $alias[1] : 'exec';
		$alias = $alias[0];
		
		$module = $this->getModuleAlias($alias);
		
		if ($class = $this->getModuleClass($module,'dynamic')) {
			$mod = $this->getModule($alias);

			ob_start();
			$args = $mod->$method($args);
			smp_Response::getInstance()->addContent(ob_get_clean());
				
			$this->_last_module = $module;

			while($next_mod = $mod->getNext())
			{
				$args = $this->runModule($next_mod,$args);
			}

			return $args;
		}
		else
		{
			throw new Exception("Module {$module} could not be found!");
		}
	}

	/**
	 * clearCurrentChain
	 * 
	 * Clears the currently executing module chain.
	 *
	 */
	public function clearCurrentChain()
	{
		$this->_chain = array();
	}

	/**
	 * addModuleToChain
	 * 
	 * Adds a module identifier to the current chain.
	 *
	 * @param $module
	 */
	public function addModuleToChain($module)
	{
		$this->_chain[] = $module;
	}

	/**
	 * getLastModule
	 * 
	 * Returns the last executed module identifier.
	 *
	 * @return string
	 */
	public function getLastModule()
	{
		return $this->_last_module;
	}

	/**
	 * getModuleAlias
	 * 
	 * Return the module for the given $alias.
	 *
	 * @param $alias
	 * @return string
	 */
	public function getModuleAlias($alias)
	{
		return isset($this->_alias[$alias]) ? $this->_alias[$alias] : $alias;
	}
	
	/**
	 * getModule
	 * 
	 * Instanciate and return an instance of the given module $alias. Optionally, only return the module class name by passing boolean
	 * boolean true on the second parameter.
	 *
	 * @param $alias string
	 * @param $class_only bool
	 * @return smp_Module|string
	 */
	public function getModule($alias,$class_only=false)
	{
		$module = $this->getModuleAlias($alias);

		$class = $this->getModuleClass($module,'dynamic');

		if ($class_only) return $class;

		if (!isset($this->_modules[$alias]) || !($this->_modules[$alias] instanceof $class))
		{
			$this->_modules[$alias] = new $class($this,$alias);
		}

		return $this->_modules[$alias];
	}

	/**
	 * getConfig
	 * 
	 * Gets the current shared config instance.
	 *
	 * @return smp_Config
	 */
	public function getConfig()
	{
		return $this->_config;
	}

	/**
	 * initEnvironment
	 * 
	 * Initialise the environment.
	 *
	 */
	private function initEnvironment()
	{
		ini_set('display_errors',1);
		error_reporting(-1);

		define('DS',DIRECTORY_SEPARATOR);
		define('PS',PATH_SEPARATOR);
		date_default_timezone_set('UTC');

		$this->_lib = isset($this->_options['lib_path']) ? realpath($this->_options['lib_path']).DS : dirname(dirname($_SERVER['SCRIPT_FILENAME'])).DS.'lib'.DS;
		$this->_app = isset($this->_options['app_path']) ? realpath($this->_options['app_path']).DS : dirname(dirname($_SERVER['SCRIPT_FILENAME'])).DS.'app'.DS;
		$this->_root = dirname(__FILE__).DS;
		
		if (!isset($this->_options['tmp_path'])) {
			$this->_tmp = realpath(sys_get_temp_dir()).DS;
		}
		else {
			$this->_tmp = realpath($this->_options['tmp_path']).DS;
		}
		
		if (!defined('TMP')) define('TMP',$this->_tmp);
		if (!defined('LIB')) define('LIB',$this->_lib);
		if (!defined('APP')) define('APP',$this->_app);
		if (!defined('ROOT')) define('ROOT',$this->_root);

		foreach ($this->_options as &$option) {
			$option = str_replace('{APP}',$this->_app,$option);
			$option = str_replace('{ROOT}',$this->_root,$option);
			$option = str_replace('{LIB}',$this->_lib,$option);
			$option = str_replace('{TMP}',$this->_tmp,$option);
		}
	}

	/**
	 * initArchive
	 * 
	 * Initialise the smp_Archive support.
	 *
	 */
	private function initArchive()
	{
		$path = $this->_root.'core'.DS.'archive'.DS.'archive.php';
		require $path;

		smp_Archive::init(TMP);
	}

	/**
	 * initTapped
	 * 
	 * Initialise the autoloader.
	 *
	 */
	private function initTapped()
	{
		$path = $this->_root.'core'.DS.'tapped'.DS.'tapped.php';
		require $path;
		$t = smp_Tapped::getInstance()
		->setCache($this->_options['tapped_cache'])->addPath($this->_lib);
		
		if (!stristr($this->_lib,$this->_root)) {
			$t->addPath($this->_root);
		}
		
		if (file_exists($this->_app)) {
			$t->addPath($this->_app);
		}
			
		$t->registerAutoloader();
	}

	/**
	 * initConfig
	 * 
	 * Initialise the configuration.
	 *
	 */
	private function initConfig()
	{
		$backend = $this->_options['config_backend'];
		$options = $this->_options['config_backend_options'];
		$options = is_array($options) ? $options : array();
		
		$path = $this->_options['config_path'];
		
		if (!defined('ENV')) define('ENV',($this->_options['environment'] ? $this->_options['environment'] : smp_Enviroment::getInstance()->detectEnvironment($this->_options['environments'])));
				
		$roots = array($path.DS.'default',$path.DS.ENV);
		
		$options['roots'] = $roots;
		
		$this->_config = new smp_Config($backend,$options);
	}
}
