<?php
class smp_Bootstrap extends smp_Module {
	
	public function exec($args = array())
	{
		$this->setDefault('tasks', array());
		
		foreach ($this->getConfig('tasks') as $task) {
			$task = "init_{$task}";
			$this->$task();
		}
	}
		
}