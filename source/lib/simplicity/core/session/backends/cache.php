<?php
/**
 * smp_SessionBackendCache
 *
 * A smp_Session backend used to store session data using smp_Cache.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_core
 */
class smp_SessionBackendCache implements smp_SessionBackend 
{
	private $_id;
	
	/**
	 * smp_Cache
	 *
	 * @var smp_Cache
	 */
	private $_cache;	
	private $_expires = 300;	
	private $_data = array();
	
	private $_defaults = array(
		'module' => 'Cache'
	);
	
	/**
	 * Constructor
	 * 
	 * Create a new smp_SessionBackendCache.
	 *  
	 * @param $options array
	 */
	public function __construct($options = array())
	{
		$options = $options + $this->_defaults;
			
		$this->_cache = Simplicity::getInstance()->getModule($options['module']);
		$this->_cache->exec(array());

		$this->_expires = isset($options['expires']) ? (int) $options['expires'] : $this->_expires;
	}
	
	/**
	 * @inherited
	 */
	public function isValidId($id)
	{
		return $this->_cache->has('_session'.$id);
	}

	/**
	 * @inherited
	 */	
	public function setId($id)
	{
		$this->_id = $id; 
		$this->_data = $this->_cache->get('_session'.$this->_id);
	}

	/**
	 * @inherited
	 */	
	public function getId()
	{
		return $this->_id;
	}
	
	/**
	 * @internal
	 */	
	public function __destruct()
	{
		$this->save();
	}
	
	/**
	 * @inherited
	 */	
	public function get($name) {
		return isset($this->_data[$name]) ? $this->_data[$name] : null;	
	}
	
	/**
	 * @inherited
	 */	
	public function set($name,$value) {
		$this->_data[$name] = $value;	
	}
	
	/**
	 * @inherited
	 */	
	public function add($name,$value) {
		if (!$this->has($name))
		{
			$this->set($name,$value);			
		}
	}
	
	/**
	 * @inherited
	 */	
	public function del($name) {
		unset($this->_data[$name]);	
	}
	
	/**
	 * @inherited
	 */	
	public function has($name) {
		return isset($this->_data[$name]);	
	}

	/**
	 * @inherited
	 */	
	public function clear() {
		$this->_data = array();
	}	
	
	/**
	 * @inherited
	 */	
	public function save() {
		$this->_cache->set('_session'.$this->_id,$this->_data,$this->_expires);
	}	
}