<?php
/**
 * smp_SessionBackend
 * 
 * Implemented by all Simplicity Session backends.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
interface smp_SessionBackend
{	
	/**
	 * isValidId
	 *
	 * Should return boolean true or false if the given session $id is valid.
	 *  
	 * @param $id string
 	 * @return boolean
	 */
	public function isValidId($id);
	
	/**
	 * setId
	 * 
	 * Should set the session id to the given $id.
	 * 
	 * @param $id string
	 */
	public function setId($id);
	
	/**
	 * getId
	 * 
	 * Should return the current session id.
	 * 
	 * @return string
	 */
	public function getId();
	
	/**
	 * get
	 * 
	 * Should retrieve the given key $name from the session.
	 * 
	 * @param $name string
	 * @return mixed
	 */
	public function get($name);

	/**
	 * set
	 * 
	 * Should set the given key $name to the given $value.
	 * 
	 * @param $name string
	 * @param $value mixed
	 */
	public function set($name,$value);
	
	/**
	 * add
	 * 
	 * Should add the given key $name to the given $value only if it does not currently exist.
	 * 
	 * @param $name string
	 * @param $value mixed
	 */	
	public function add($name,$value);

	/**
	 * del
	 * 
	 * Should delete the given key $name from the session.
	 * 
	 * @param $name string
	 */	
	public function del($name);

	/**
	 * has
	 * 
	 * Should return boolean true or false if the given key $name exists in the session.
	 * 
	 * @param $name string
	 */		
	public function has($name);	
	
	/**
	 * has
	 * 
	 * Should clear the current session.
	 * 
	 */		
	public function clear();	
	
	/**
	 * save
	 * 
	 * Should save the current session to whatever is being used to store the sessions.
	 * 
	 */		
	public function save();	
	
}