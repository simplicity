<?php

/**
 * Session Module
 *
 * The Simplicity Session module is a core module providing a session services to Simplicity 
 * and other modules.  
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_children smp_SessionBackend
 * @smp_core
 */
class smp_SessionModule extends smp_Module
{

	/**
	 * smp_SessionBackend
	 *
	 * @var smp_SessionBackend
	 */
	private $_backend;
	private $_name;
	
	/**
	 * @inherited
	 */
	public function init() 
	{
		$this->createExtensionPoint('smp_SessionBackend');
		$this->registerPluginPrefix('smp_');
	}

	/**
	 * @inherited
	 */	
	public function exec($args=array())
	{	
		$backend = $this->getConfig('backend.module'); 
		$backend = $backend ? $backend : 'Cache'; 
		
		$options = $this->getConfig('backend.options');
		$options = is_array($options) ? $options : array();
				
		if (!$class = $this->getModuleClass($backend)) throw new Exception("Unknown session backend {$backend}");
		
		$this->_name = md5(smp_Enviroment::getInstance()->getServer('SCRIPT_FILENAME').__FILE__);
		$this->_backend = new $class($options);
		
		$this->start();
	}
	
	private function start()
	{
		$id = smp_Request::getInstance()->getCookie($this->_name);
		
		if (!$this->_backend->isValidId($id))
		{
			$this->regenerateId();
		}
		else {
			$this->_backend->setId($id);
		}
	}

	/**
	 * getId
	 * 
	 * Returns the current session id.
	 * 
	 * @return string
	 */
	public function getId()
	{
		return $this->_backend->getId();
	}

	/**
	 * regenerateId
	 * 
	 * Regenerate the session id and return the new ID. 
	 * 
	 * @return string
	 */
	public function regenerateId()
	{
		$id = md5(microtime().rand(0,1000));
		smp_Response::getInstance()->setCookie($this->_name,$id);
		$this->_backend->setId($id);

		return $id;
	}

	/**
	 * get
	 * 
	 * Retrieves the given key $name from the session.
	 * 
	 * @param $name string
	 * @return mixed
	 */
	public function get($name)
	{
		return $this->_backend->get($name);
	}

	/**
	 * set
	 * 
	 * Sets the given key $name to the given $value.
	 * 
	 * @param $name string
	 * @param $value mixed
	 */
	public function set($name,$value)
	{
		return $this->_backend->set($name,$value);
	}

	/**
	 * add
	 * 
	 * Adds the given key $name to the given $value only if it does not currently exist.
	 * 
	 * @param $name string
	 * @param $value mixed
	 */		
	public function add($name,$value)
	{
		return $this->_backend->add($name,$value);
	}

	/**
	 * del
	 * 
	 * Deletes the given key $name from the session.
	 * 
	 * @param $name string
	 */		
	public function del($name)
	{
		return $this->_backend->del($name);
	}

	/**
	 * has
	 * 
	 * Returns boolean true or false if the given key $name exists in the session.
	 * 
	 * @param $name string
	 */			
	public function has($name)
	{
		return $this->_backend->has($name);
	}
	
	/**
	 * clear
	 * 
	 * Clears the current session.
	 * 
	 */
	public function clear()
	{
		return $this->_backend->clear();
	}
	
	/**
	 * save
	 * 
	 * Saves the current session to whatever is being used to store them.
	 * 
	 */
	public function save()
	{
		return $this->_backend->save();
	}
}