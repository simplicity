<?php

/**
 * Cache Backend Interface
 *
 * Implemented by cache backends.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_core
 */
interface smp_CacheBackend
{
	
	/**
	 * get
	 * 
	 * Should get a key from the cache with the given $name or return false if unset.
	 * 
	 * @param $name string
	 * @return mixed
	 */
	public function get($name);

	/**
	 * set
	 * 
	 * Should set a key in the cache to the given $name and $value with an optional timeout $ttl in seconds, this defaults to 0 so infinite.
	 * 
	 * @param $name string
	 * @return mixed
	 */	
	public function set($name,$value,$ttl=0);
	
	/**
	 * add
	 * 
	 * Should set the key only if it does not exist presently. 
	 * 
	 * @see set
	 * @param $name string
	 * @return mixed
	 */
	public function add($name,$value,$ttl=0);
	
	/**
	 * del
	 * 
	 * Should delete a key from the cache.
	 * 
	 * @param $name string
	 * @return mixed
	 */
	public function del($name);
	
	/**
	 * has
	 * 
	 * Should return true or false if a key exists in the cache.
	 * 
	 * @param $name string
	 * @return mixed
	 */	
	public function has($name);
	
	/**
	 * clear
	 * 
	 * Should empty all the keys in the cache.
	 * 
	 */
	public function clear();
}