<?php

/**
 * APC Cache Backend
 *
 * Provides an APC cache backend for smp_Cache
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_core
 */
class smp_CacheBackendApc implements smp_CacheBackend
{
	
	private $_index = array();
	
	/**
	 * If the apc extension is loaded then the load priority is 1, otherwise disable this module.
	 * 
	 * @return int
	 */
	static public function loadPriority()
	{
		if (!extension_loaded('apc')) return 0;
		return 1;
	}
	
	/**
	 * @inherited
	 */
	public function get($name)
	{
		$ret = apc_fetch($name,$success);
		if (!$success) return null;
		if (isset($ret['timeout']) && $ret['timeout'] < time()) {
			$this->del($name);
			return null;	
		}	
		return isset($ret['data']) ? $ret['data'] : $ret;
	}

	/**
	 * @inherited
	 */
	public function has($name)
	{
		apc_fetch($name,$success);
		return $success;
	}

	/**
	 * @inherited
	 */
	public function del($name)
	{
		return apc_delete($name);
	}

	/**
	 * @inherited
	 */
	public function set($name,$value,$ttl=0)
	{
		$ttl = $ttl ? (time() + $ttl) : 0;
		$data = array('data'=>$value,'timeout'=>$ttl);
		return apc_store($name,$data);
	}

	/**
	 * @inherited
	 */
	public function add($name,$value,$ttl=0)
	{
		$ttl = $ttl ? (time() + $ttl) : 0;
		$data = array('data'=>$value,'timeout'=>$ttl);
		return apc_add($name,$data);
	}
	
	/**
	 * @inherited
	 */
	public function clear()
	{
		return false;
	}

}