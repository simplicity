<?php

/**
 * SQLite Cache Backend
 *
 * Provides a SQLite based cache backend for smp_Cache
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_core
 */
class smp_CacheBackendSqlite implements smp_CacheBackend
{

	/**
	 * If available loaded but has lower priority than apc.
	 * 
	 * @return int
	 */
	static public function loadPriority()
	{
		if (!extension_loaded('pdo_sqlite')) return 2;
	}

	private $_pdo;	
	private $_qry = array();
	
	private $_queries = array(
		'ins' => 'insert into `{TABLE}` (`key`,`value`,`timeout`) values (?,?,?)',
		'upd' => 'update `{TABLE}` set `value` = ?,`timeout` = ? where `key` = ?',
		'del' => 'delete from `{TABLE}` where `key` = ?',
		'get' => 'select `value`,`timeout` from `{TABLE}` where `key` = ? and (`timeout` = -1 or `timeout` >= strftime("%s","now")) limit 1',
		'has' => 'select count(*) from `{TABLE}` where `key` = ? and (`timeout` = -1 or `timeout` >= strftime("%s","now")) limit 1',
		'clear' => 'delete from `{TABLE}`',
		'gc' => 'delete from `{TABLE}` where `timeout` != -1 and `timeout` <= strftime("%s","now")'
	);

	public function __construct($options) 
	{
		$dsn = isset($options['dsn']) ? $options['dsn'] : 'sqlite::memory:';
		$this->_pdo = new PDO($dsn,null,null,array(PDO::ATTR_PERSISTENT => true,PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
		$this->_table = md5(smp_Enviroment::getInstance()->getServer('SCRIPT_FILENAME'));		
		
		$sql = "create table if not exists `{$this->_table}` (key primary key,value,timeout int)";
		$this->_pdo->exec($sql);

		$this->compileQueries();
	}
	
	private function compileQueries()
	{
		foreach ($this->_queries as $type => $qry) {
			$this->_qry[$type] = $this->_pdo->prepare(str_replace('{TABLE}',$this->_table,$qry));
		}
	}
	

	public function set($key,$value,$timeout=null) 
	{		
		$value = serialize($value);
		
		if (!isset($timeout)) {
			$timeout = -1;	
		}
		else {
			$timeout = time() + $timeout;
		}
		
		if ($this->has($key)) {
			return $this->execQuery('upd',array($value,$timeout,$key));	
		}
		else {
			return $this->execQuery('ins',array($key,$value,$timeout));
		}			
	}
	
	public function add($key,$value,$timeout=null) 
	{
		if (!$this->has($key)) {
			return $this->set($key,$value,$timeout);
		}
		
		return false;
	}
	
	public function get($key) 
	{
		if ($this->execQuery('get',array($key)) && count($this->getQuery('get'))) {
			return unserialize($this->getQuery('get')->fetchColumn(0));
		}
		
		return false;
	}

	public function del($key) 
	{
		return $this->execQuery('del',array($key));	 
	}
	
	public function has($key)
	{
		if ($this->execQuery('has',array($key)))
		{
			return (bool) $this->getQuery('has')->fetchColumn(0);
		}

		return false;
	}
	
	public function clear()
	{
		return $this->execQuery('clear');
	}
	
	public function __destruct()
	{
		if (rand(1,100) <= 20) {
			$this->execQuery('gc');
		}
	}
	
	private function getQuery($type)
	{
		return $this->_qry[$type];
	}
	
	private function execQuery($type,array $params = array(),$rec=false)
	{
		try {
			$this->_qry[$type]->execute($params);	
		}
		catch (PDOException $e) {
			if ($rec) throw $e;
			
			if (!$rec && stristr($e->getMessage(),'database schema has changed')) {
				$this->compileQueries();
				return $this->execQuery($type,$params,true);
			}	
		}
		
		return $this->_qry[$type];
	}

}