<?php

/**
 * Memcache Cache Backend
 *
 * Provides a Memcache based cache backend for smp_Cache
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_core
 */
class smp_CacheBackendMemcache implements smp_CacheBackend
{

	/**
	 * Not loaded unless specifically requested as will require further configuration.
	 * 
	 * @return int
	 */
	static public function loadPriority()
	{
		return 0;
	}

	private $_mc;

	public function __construct($options) 
	{
		$this->_mc = new Memcache();
		
		$def = array(
			'port' => 11211,
			'weight' => 1
		);
		
		$options['servers'] = isset($options['servers']) ? $options['servers'] : array('127.0.0.1');
		
		foreach ($options['servers'] as $host => $server) {
			if (is_numeric($host)) {
				$this->_mc->addServer($server,11211);
			}
			else {
				$server = $server + $def;
				$this->_mc->addServer($server,$server['port'],true,$server['weight']);
			}
		}
		
	}

	public function set($key,$value,$timeout=null) 
	{		
		$this->_mc->set($key,$value,null,$timeout);
	}
	
	public function add($key,$value,$timeout=null) 
	{
		$this->_mc->add($key,$value,null,$timeout);
	}
	
	public function get($key) 
	{
		$ret = $this->_mc->get($key);
		return ($ret === false) ? false : $ret;
	}

	public function del($key) 
	{
		$this->_mc->delete($key);
	}
	
	public function has($key)
	{
		return ($this->get($key) === false) ? false : true;
	}
	
	public function clear()
	{
		return false;
	}
	
}