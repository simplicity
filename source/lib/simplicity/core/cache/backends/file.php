<?php

/**
 * File Cache Backend
 *
 * Provides a file based cache backend for smp_Cache
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_core
 */
class smp_CacheBackendFile implements smp_CacheBackend  
{
	
	/**
	 * Should always be available, but not a favourite.
	 * 
	 * @return int
	 */
	static public function loadPriority()
	{
		return 3;	
	}
	
	private $_tmp;
	private $_options = array(
		'default_timeout' => 86400,
		'cleanup_chance' => 10,
	);
	
	/**
	 * __construct
	 * 
	 * Create a new smp_CacheBackendFile
	 * 
	 * @param $options array
	 */
	public function __construct($options=array())
	{
		$this->_options = array_merge($this->_options,$options);
		$ns = md5(smp_Enviroment::getInstance()->getServer('SCRIPT_FILENAME').__FILE__);
		$tmp = defined('TMP') ? TMP : sys_get_temp_dir().DS;
		
		if (substr($tmp,-1,1) !== DS) $tmp .= DS;
		$this->_tmp = $tmp.$ns.'.cache'.DS;	
	}
	
	/**
	 * getFilePath
	 *
	 * Get the path for the given key $name.
	 * 
	 * @param $name string
	 * @return string
	 */
	private function getFilePath($name)
	{
		$name = md5($name);
		$path = $this->_tmp.implode(DS,sscanf($name,'%1s%2s%3s'));
		if (!file_exists($path)) mkdir($path,0700,true);
		return $path.DS.$name.'.php';
	}
	
	/**
	 * getLock
	 * 
	 * Get a lock on the given $file, optionally get an exclusive lock (for writing) by passing true as the second parameter. 
	 * You may also specify the $max_timeout in microseconds to wait for a lock. Returns true on success or false on failure.
	 * 
	 * @param $file string
	 * @param $exclusive boolean
	 * @param $max_timeout int
	 * @return boolean
	 */
	private function getLock($file,$exclusive=false,$max_timeout=250000)
	{
		$lock = $file.'.lock';
		$wait = 0;
		while(file_exists($lock)) { 
			usleep(250);
			if (($wait += 250) > $max_timeout) return false; 
		}
		
		if (!$exclusive) return true;
		
		return touch($lock);
	}

	/**
	 * unLock
	 * 
	 * Remove an exclusive lock for the given $file.
	 * 
	 * @param $file string
	 * @return boolean
	 */
	private function unLock($file)
	{
		$lock = $file.'.lock';
		return unlink($lock);
	}	
	
	/**
	 * @inherited
	 */
	public function set($name,$value,$ttl=0)
	{
		$file = $this->getFilePath($name);

		if (!$this->getLock($file,true)) throw new smp_Exception('File cache cannot create lock.');
		
		$data = array('timeout' => ($ttl ? time() + $ttl : false),'data' => $value);
		$data =  var_export($data,true);
		$code = "<?php return {$data}; ?>";
		
		$ret = file_put_contents($file,$code);
		
		$this->unLock($file);
		
		return $ret;
	}

	/**
	 * @inherited
	 */
	public function add($name,$value,$ttl=0)
	{
		if (!$this->has($name)) {
			$this->set($name,$value,$ttl);
		}
	}
	
	/**
	 * @inherited
	 */
	public function get($name)
	{
		$file = $this->getFilePath($name);
		if (!file_exists($file)) return null;
		if (!$this->getLock($file)) throw new smp_Exception('File cache cannot create lock.');
		
		if (!($data = @include($file))) return null;
		if (!isset($data['timeout']) || !isset($data['data'])) return null;
		if ($data['timeout'] && time() > $data['timeout'])
		{
			unlink($file);
			return null;
		}

		return $data['data'];
	}
	
	/**
	 * @inherited
	 */
	public function del($name)
	{
		$file = $this->getFilePath($name);
		if (!file_exists($file)) return null;
		if (!$this->getLock($file,true)) throw new smp_Exception('File cache cannot create lock.');
		unlink($file);
		$this->unLock($file);
	}
	
	/**
	 * @inherited
	 */
	public function has($name)
	{
		return (boolean) $this->get($name);
	}
	
	public function clear()
	{
		return false;
	}
}