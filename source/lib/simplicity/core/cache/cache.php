<?php

/**
 * Cache Module
 *
 * The Simplicity Cache module is a core module providing a cache abstraction layer to Simplicity 
 * and other modules.  
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_children smp_CacheBackend
 * @smp_core
 */
class smp_CacheModule extends smp_Module
{

	private $_ns;

	/**
	 * smp_CacheBackend
	 *
	 * @var smp_CacheBackend
	 */
	private $_backend;

	/**
	 * @inherited
	 */
	public function init() 
	{
		$this->createExtensionPoint('smp_CacheBackend');
		$this->registerPluginPrefix('smp_');

		$this->_ns = md5(smp_Enviroment::getInstance()->getServer('SCRIPT_FILENAME').__FILE__);	
		
		$this->setDefault('backend.module','auto');
		$this->setDefault('backend.options',array());
	}

	/**
	 * @inherited
	 */
	public function exec($args=array())
	{
		$this->loadBackend();
	}
	
	/**
	 * loadBackend
	 * 
	 * Load the selected backend, or automatically load the backend module based on their loadPriority.
	 * 
	 */
	private function loadBackend()
	{	
		$backend = $this->getConfig('backend.module');
		$options = $this->getConfig('backend.options');
		
		if ($backend == 'auto')
		{
			$backends = $this->getModules();
			$beds = array();
			foreach ($backends as $module => $class)
			{
				if ($pri = call_user_func(array($class,'loadPriority')))
				{
					$beds[$module] = $pri;
				}
			}

			arsort($beds,SORT_NUMERIC);

			$backend = key($beds);
		}

		if (!$class = $this->getModuleClass($backend)) throw new Exception("Unknown cache backend {$backend}");
				
		$this->_backend = new $class($options);
	}

	/**
	 * get
	 * 
	 * Retrieve the given key $name from the cache.
	 * 
	 * @param $name string
	 * @return mixed
	 */
	public function get($name)
	{
		$name = "{$this->_ns}.$name";
		return $this->_backend->get($name);
	}

	/**
	 * set
	 * 
	 * Set the key $name to the given $value, optionally providing a timeout $ttl in seconds (defaults to 0; no timeout)
	 * 
	 * @param $name string
	 * @param $value mixed
	 * @param $ttl integer
	 */
	public function set($name,$value,$ttl=0)
	{
		if (!$name) return;
		$name = "{$this->_ns}.$name";
		return $this->_backend->set($name,$value,$ttl);
	}

	/**
	 * add
	 * 
	 * Set the given key only if it does not exist.
	 * 
	 * @see set
	 * @param $name string
	 * @param $value mixed
	 * @param $ttl integer
	 */
	public function add($name,$value,$ttl=0)
	{
		if (!$name) return;
		$name = "{$this->_ns}.$name";
		return $this->_backend->add($name,$value,$ttl);
	}

	/**
	 * del
	 * 
	 * Delete the given key $name.
	 * 
	 * @param $name string
	 */
	public function del($name)
	{
		if (!$name) return;
		$name = "{$this->_ns}.$name";
		return $this->_backend->del($name);
	}

	/**
	 * has
	 * 
	 * Returns true or false if the given key $name is in the cache.
	 * 
	 * @param $name string
	 * @return boolean
	 */
	public function has($name)
	{
		$name = "{$this->_ns}.$name";
		return $this->_backend->has($name);
	}
	
	public function clear()
	{
		return $this->_backend->clear();
	}
}