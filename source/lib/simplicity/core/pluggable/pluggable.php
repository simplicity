<?php
/**
 * smp_Pluggable
 *
 * The core class of the Simplicity framework. Provides the ability to add 'extension points'
 * to any class that extends it. This involves specifying an interface or base class that your
 * plugins will use, Pluggable will utilise the class index in Tapped to find available plugins.
 *
 * Pluggable plugin names are created by taking the base class or interface name, and
 * prepending that with the module name. So if you use a base class called MyBase your plugin
 * class name will be called MyPluginMyBase, the plugin name will still be MyPlugin.
 *
 * You may optionally specify any prefix(s) that you use to namespace your classes to your classes
 * and Pluggable will remove the prefix from base and plugin classes. For example, all Simplicity
 * classes are prefixed with 'smp_' so my base class, now called smp_MyBase and my plugin
 * smp_MyPluginMyBase still produce a module named MyPlugin.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @see smp_Tapped
 * @smp_core
 */
abstract class smp_Pluggable
{
	private $_extension_points = array();
	private $_modules = array();

	/**
	 * createExtensionPoint
	 * 
	 * Create an extension point using the provided $base class or interface and optionally specify a
	 * string $name for this extension point. If you will be using more than one extension point you must
	 * specify a name.
	 *
	 * @param $base string
	 * @param $name string
	 * @return void
	 */
	final protected function createExtensionPoint($base,$name=null)
	{
		if (!isset($name)) {
			if (!count($this->_extension_points)) {
				$name = '_default';
			} else {
				throw new Exception("Must specify a name when using multiple extension points.");
			}
		}

		if ($this->hasExtensionPoint($name)) return;

		if (!$type = smp_Tapped::getInstance()->getType($base)) throw new Exception("Attemtped to create extension point {$name} using non-existant base {$base}.");

		$this->_extension_points[$name] = array('type'=>$type,'base'=>$base);
	}

	/**
	 * hasExtensionPoint
	 * 
	 * Returns true / false if the given extension point exists.
	 *
	 * @param $name string
	 * @return boolean
	 */
	final protected function hasExtensionPoint($name="")
	{
		if (!isset($name) && count($this->_extension_points) == 1) {
			$name = '_default';
		}
		return isset($this->_extension_points[$name]);
	}

	/**
	 * registerPluginPrefix
	 * 
	 * Set a prefix to be removed from base and plugin class names when resolving
	 * plugin names.
	 *
	 * @param $prefix
	 * @param $name string
	 * @return void
	 */
	final protected function registerPluginPrefix($prefix,$name=null)
	{
		if (!isset($name) && count($this->_extension_points) == 1) {
			$name = '_default';
		}
		if (!$this->_extension_points[$name]) return false;
		$this->_extension_points[$name]['prefix'][$prefix] = $prefix;
	}

	/**
	 * getModules
	 * 
	 * Return the names of all the modules configured for this class. You can
	 * optionally provide an extension point name.
	 *
	 * @param $name string
	 * @return array
	 */
	final public function getModules($name=null)
	{		
		if (!isset($name) && count($this->_extension_points) == 1) {
			$name = '_default';
		}

		if (!isset($this->_modules[$name]))
		{
			if (!isset($this->_extension_points[$name])) return false;
				
			if (!$def = $this->_extension_points[$name]) return false;

			$classes = smp_Tapped::getInstance()->getChildren($def['base']);
			
			if (!$def['type']) throw new Exception("Extension point {$name} has no base defined.");

			$out = array();

			$base = $def['base'];
			if (isset($def['prefix']))
			{
				$base = str_replace($def['prefix'],array(),$base);
			}

			while($class = array_shift($classes))
			{
				if (isset($def['prefix']))
				{
					$replace = $def['prefix'];
					$replace[] = $base;
				} else {
					$replace = array($base);
				}

				$module = str_replace($replace,array(),$class);
				$out[$module] = $class;
			}

			$this->_modules[$name] = $out;
		}

		return $this->_modules[$name];
	}

	/**
	 * getClassModule
	 * 
	 * Return the module name for the given $class and optional extension point $name.
	 *
	 *
	 * @param $class string
	 * @param $name string
	 * @return string
	 */
	final public function getClassModule($class,$name=null)
	{
		if (!isset($name) && count($this->_extension_points) == 1) {
			$name = '_default';
		}

		if (!$def = $this->_extension_points[$name]) return false;

		$classes = $this->getModules($name);

		return array_search($class,$classes);
	}

	/**
	 * hasModule
	 * 
	 * Return boolean true / false if the given $module (and optional extension point $name) is available.
	 * 
	 * @param string $module
	 * @return boolean
	 */
	final public function hasModule($module,$name=null)
	{
		if (!isset($name) && count($this->_extension_points) == 1) {
			$name = '_default';
		}
		
		if (!isset($this->_extension_points[$name]) || !$def = $this->_extension_points[$name]) return false;

		$classes = $this->getModules($name);
		return isset($classes[$module]);
	} 
	
	/**
	 * getModuleClass
	 * 
	 * Return the class name for the given $module and optional extension point $name.
	 *
	 *
	 * @param $module string
	 * @param $name string
	 * @return string
	 */
	final public function getModuleClass($module,$name=null)
	{
		if (!isset($name) && count($this->_extension_points) == 1) {
			$name = '_default';
		}
		if (!$def = $this->_extension_points[$name]) return false;
		$classes = $this->getModules($name);
		return isset($classes[$module]) ? $classes[$module] : false;
	}

}