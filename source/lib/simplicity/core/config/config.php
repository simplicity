<?php
/**
 * smp_Config
 *
 * Provides a unified interface to application configuration. Can use a variety of backends, although currenty there is only
 * support PHP array based config.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_children smp_ConfigBackend
 * @smp_core
 */
class smp_Config extends smp_Pluggable
{
	/**
	 * smp_ConfigBackend
	 *
	 * @var smp_ConfigBackend
	 */
	private $_backend;

	private $_data = array();
	
	private $_cache = false;
	
	/**
	 * Constructor
	 * 
	 * Create a new smp_Config instance, specifying the $backend module to use and providing an associative array of config $options.
	 *
	 * @param $backend string
	 * @param $options array
	 */
	public function __construct($backend,$options=array())
	{
		$this->createExtensionPoint('smp_ConfigBackend');
		$this->registerPluginPrefix('smp_');

		if (!$class = $this->getModuleClass($backend)) throw new Exception("Unknown config backend {$backend}");

		$this->_backend = new $class($options);
		
		$this->_cache = isset($options['cache_timeout']) ? (int) $options['cache_timeout'] : 0;
		
		$path = defined('TMP') ? TMP : sys_get_temp_dir().DS;
		$file = $path.md5(smp_Enviroment::getInstance()->getServer('SCRIPT_FILENAME').__FILE__).'.smpconfig';
		
		if (!$this->_data = $this->getCacheFromFile($file)) {
			$this->_data = $this->_backend->readData();
			$this->setCacheToFile($this->_data,$file);
		}
	}

	/**
	 * setCacheToFile
	 * 
	 * Save the given $data to the $file with the given $timeout.
	 * Returns true on success or false on failure.
	 * 
	 * @param $data mixed
	 * @param $file string
	 * @param $timeout integer
	 * @return boolean
	 */
	private function setCacheToFile($data,$file)
	{
		if (!$this->_cache) return true;
		$data = array('timeout'=>time()+$this->_cache,'data'=>$data);
		$data =  var_export($data,true);
		$code = "<?php return {$data}; ?>";
		return file_put_contents($file,$code);
	}

	/**
	 * getCacheFromFile
	 * 
	 * Retrieve the cached data from the given $file.
	 * 
	 * @param $file string
	 * @return mixed
	 */
	private function getCacheFromFile($file)
	{
		if (!$this->_cache) return null;
		if (!($data = @include($file))) return null;
		if (!isset($data['timeout']) || !isset($data['data'])) return null;
		if (time() > $data['timeout'])
		{
			unlink($file);
			return null;
		}

		return $data['data'];
	}	
	
	/**
	 * get
	 * 
	 * Return a the value of a given configuration node for the given $path.
	 *
	 * @param $path string
	 * @return mixed
	 */
	public function get($path=null)
	{
		if (!isset($path)) return $this->_data;

		$spl = explode('.',$path);
		$last = $this->_data;
		while ($item = array_shift($spl))
		{
			if (!isset($last[$item])) return null;
			$last = $last[$item];
		}
		return $last;
	}


	/**
	 * has
	 * 
	 * Returns boolean true / false if a given path exists.
	 *
	 * @param $path
	 * @return boolean
	 */
	public function has($path)
	{
		$spl = explode('.',$path);
		$last = $this->_data;
		while ($item = array_shift($spl))
		{
			if (!isset($last[$item])) return false;
			$last = $last[$item];
		}
		return true;
	}
}