<?php

/**
 * smp_ConfigBackendArray
 *
 * Provides a config backend to PHP array format files.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * @smp_core
 */
class smp_ConfigBackendArray implements smp_ConfigBackend
{
	private $_roots = array();
	
	/**
	 * Constructor
	 * 
	 * Creates a new array config backend
	 *
	 * @param $options
	 */
	public function __construct($options=array())
	{
		if (!isset($options['roots'])) return false;
		
		$this->_roots = is_array($options['roots']) ? $options['roots'] : array($options['roots']);
	}

	/**
	 * arrayMerge
	 * 
	 * Merge the given array $a with $b overriding any values in $a that exist in $b.
	 * 
	 * @param $a array
	 * @param $b array
	 * @return array
	 */
	private function arrayMerge(array $a,array $b)
	{
		foreach ($a as $k => $v) {
			if (is_array($a[$k])) {
				if (isset($b[$k])) {
					if (is_array($b[$k])) {
						if (isset($b[$k][0])) {
							$a[$k] = $b[$k];
						}
						else {
							$a[$k] = $this->arrayMerge($a[$k],$b[$k]);	
						}						
					}
					else {
						$a[$k] = $b[$k];
					}
				}
			}
			elseif (isset($b[$k])) {
				$a[$k] = $b[$k];	
			}
		}
	
		foreach ($b as $k => $v) {
			if (!isset($a[$k])) $a[$k] = $b[$k];
		}
		
		return $a;
	}
	
	/**
	 * @inherited
	 */
	public function readData()
	{
		$data = array();
		foreach ($this->_roots as $root) {
			if (!$root = realpath($root)) continue;
			$config = $this->loadConfig($root);
			if ($data) {
				$data = $this->arrayMerge($data,$config);	
			}		
			else {
				$data = $config;
			}
		}
		return $data;
	}
	
	/**
	 * loadConfig
	 * 
	 * Load the configuration from the specified $path.
	 *
	 * @param $root 
	 */
	private function loadConfig($root)
	{
		$it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($root));
		$config = array();
		foreach ($it as $file)
		{
			$spl = explode('.',$file->getFilename());
			if (array_pop($spl) != 'php') continue;
			
			if (!($data = $this->parseFile($file->getPathname(),$root))) continue;
						
			$config = $this->arrayMerge($config,$data);	
		}
		return $config;
	}

	/**
	 * getPath
	 * 
	 * Return the config path for a file.
	 *
	 * @param $file string
	 * @param $path array
	 * @return array
	 */
	private function getPath($file,$root,$path=array())
	{
		if (!count($path) && stristr($file,'.'))
		{
			$name = explode('.',basename($file));
			array_pop($name);
			$name = implode($name);
			$path[] = $name;
			$file = dirname($file);
		}

		if ($file == $root)
		{
			$path = array_reverse($path);
			return $path;
		}

		$path[] = basename($file);
		return $this->getPath(dirname($file),$root,$path);
	}

	/**
	 * parseFile
	 * 
	 * Parse a config file returning the contained configuration as an array.
	 *
	 * @param $file string
	 * @return array
	 */
	private function parseFile($file,$root)
	{
		if (!($data = include $file)) return false;

		$path = $this->getPath($file,$root);

		$out = array();
		$last =&  $out;
		foreach ($path as $key)
		{
			$last[$key] = array();
			$last =& $last[$key];
		}

		$last = $data;

		return $out;
	}
}