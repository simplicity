<?php
/**
 * smp_ConfigBackend
 *
 * Implemented by all config backends.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @see smp_Config
 * 
 * @smp_core
 */
interface smp_ConfigBackend
{
	/**
	 * Read the config data and returning an array data structure.
	 *
	 * @return array
	 */
	public function readData();
	
}