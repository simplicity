<?php
/**
 * smp_ClassMetaData
 *
 * Used to extract meta data in the form of doc comments from classes. 
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_children smp_MetaTag
 * @smp_core
 */
class smp_ClassMetaData extends smp_Pluggable {
	
	private $_comments;
	
	private $_title;
	private $_description;
	private $_tags = array();
	private $_parsed = false;
	
	protected $_class;
	
	private $_reflection;
	
	/**
	 * Constructor
	 * 
	 * Create a new smp_ClassMetaData instance, your should pass the class name as the first parameter. 
	 * 
	 */
	final public function __construct($class)
	{
		$this->createExtensionPoint('smp_MetaTag');	
		$this->registerPluginPrefix('smp_');

		if (!smp_Tapped::getInstance()->exists($class)) return false;
		
		$this->_class = $class;
		
		$this->_reflection = $this->getReflector();
	}
	
	/**
	 * getReflector
	 * 
	 * Return the ReflectionClass instance for the given entity.
	 * 
	 * @return ReflectionClass
	 */
	public function getReflector()
	{
		$ref = new ReflectionClass($this->_class);
		return $ref;
	}
	
	/**
	 * getTag
	 * 
	 * Retrieve the value of the given tag $name.
	 * 
	 * @param $name string
	 */
	final public function getTag($name) {
		if (!$this->_parsed) $this->parse();
 		return isset($this->_tags[$name]) ? $this->_tags[$name] : false;
	}
	
	/**
	 * getTags
	 * 
	 * Retrieve an array of tags on a class
	 * 
	 * @return array
	 */
	final public function getTags()
	{
		if (!$this->_parsed) $this->parse();
		return $this->_tags;
	}
	
	/**
	 * getTitle
	 * 
	 * Retrieve the doc comment title.
	 * 
	 * @return string
	 */
	final public function getTitle() {
		if (!$this->_parsed) $this->parse();
		return $this->_title;
	}
	
	/**
	 * getDescription
	 * 
	 * Retrieve the doc comment description.
	 * 
	 * @return string
	 */
	final public function getDescription() {
		if (!$this->_parsed) $this->parse();
		return $this->_description;
	}
	
	/**
	 * getMethods
	 * 
	 * Retrieve an array of methods in the class. Pass in true as the first parameter to include private methods.
	 * 
	 * @param $include_private boolean
	 * @return array
	 */
	final public function getMethods($include_private=true) {
		$methods = array();
		if (!($this->_reflection instanceof ReflectionClass)) return array();
		foreach ($this->_reflection->getMethods() as $method)
		{
			if (!$include_private) if ($method->isPrivate()) continue;
			$methods[$method->name] = $method->name;
		}
		return array_keys($methods);
	}
	
	/**
	 * getMethod
	 * 
	 * Return an smp_MethodMetaData object for the given $method.
	 * 
	 * @param $method string
	 * @return smp_MethodMetaData
	 */
	final public function getMethod($method) {
		return new smp_MethodMetaData($this->_class,$method);
	}
	
	final private function parse()
	{
		if (!$this->_class) return false;
		
		$this->extractComments();
		$this->parseTitleDescription($this->_comments);
		$this->parseTags($this->_comments);
		
		$this->_parsed = true;
	}
	
	final private function extractComments()
	{
		if (!$comments = $this->_reflection->getDocComment()) return false;
				
		$comments = explode("\n",$comments);
		array_shift($comments);
		
		$this->_comments = $comments;	
	} 

	final private function parseTitleDescription($comments)
	{
		if (!$comments) return;
		$this->_title = array_shift($comments);
		
		preg_match('/\*\s+(.*)/',$this->_title,$match);
		$this->_title = $match[1];
				
		array_shift($comments);

		$desc = "";

		while($comment = array_shift($comments))
		{
			if (!preg_match('/\*\s+(.*)/',$comment,$match)) continue;

			$comment = $match[1];

			if (isset($comments[0]) && preg_match('/\*\s+(.*)/',$comments[0],$next) && substr($next[1],0,1) == '@')
			{
				break;
			}

			$comment = trim($comment);

			$desc .= strlen($comment) ? $comment : "\n";
		}

		$this->_description = trim($desc);
	} 	
	
	final private function parseTags($comments)
	{
		if (!$comments) return;
		$tags = array();
		
		while($comment = array_shift($comments)) {
			if (!preg_match('/\*\s+@([a-z_\-0-9]+)\s*(.*)/',$comment,$match)) continue;
			$name = trim($match[1]);
			$value = trim($match[2]);
		
			$m = null;
			
			$module = smp_String::camelize($name,true);
			
			if($class = $this->getModuleClass($module)) {
				$m = new $class();
				$value = $m->parse($value);	
			}
				
			if (isset($tags[$name]))
			{
				if (!is_array($tags[$name]) || (is_array($tags[$name]) && !isset($tags[$name][0])))
				{
					$tags[$name] = array($tags[$name]);
				}
				$tags[$name][] = $value;
			}
			else {
				if (($m instanceof smp_MetaTag) && $m->hasMultipleValues()) {
					$tags[$name] = array($value);
				} elseif (!$value) {
					$tags[$name] = true;
				} else {
					$tags[$name] = $value;
				}
			}
		}	
		
		$this->_tags = $tags;
	}
}