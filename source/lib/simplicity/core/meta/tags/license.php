<?php
/**
 * smp_LicenseMetaTag
 *
 * Class extends the smp_LinkMetaTag class to add support for the @licence tag.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_LicenseMetaTag extends smp_LinkMetaTag {} 