<?php
/**
 * smp_SmpChildrenMetaTag
 *
 * Adds support for the @smp_children tag. 
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_SmpChildrenMetaTag extends smp_MetaTag
{
	/**
	 * @inherited
	 */	
	public function parse($value)
	{
		$spl = explode(' ',$value);
		$class = array_shift($spl);
		$children = array_keys(smp_Tapped::getInstance()->getChildren($class));

		if (!$children) return;
		
		foreach ($children as $k => $child) {
			$c = new smp_ClassMetaData($child);
			$children[$child] = array(
				'title' => $c->getTitle(),
				'description' => $c->getDescription(),
				'tags' => $c->getTags()
			);
			unset($children[$k]);
		}
			
		$ep = array_shift($spl);
		$value = array(
			'class' => $class,
			'ep' => $ep,
			'children' => $children
		);
		
		return $value;
	}
	
} 