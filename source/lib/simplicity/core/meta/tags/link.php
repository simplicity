<?php
/**
 * smp_LinkMetaTag
 *
 * Adds support for the @link tag.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_LinkMetaTag extends smp_MetaTag
{
	
	protected $_multiple = false;
	
	/**
	 * @inherited
	 */	
	public function parse($value)
	{
		$spl = explode(' ',$value);
		$url = array_shift($spl);
		$name = implode(' ',$spl);
		$value = array(
			'name' => $name,
			'url' => $url
		);
		
		return $value;
	}
	
} 