<?php
/**
 * smp_SmpDependsMetaTag
 *
 * Adds support for the @smp_depends tag. 
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_SmpDependsMetaTag extends smp_MetaTag
{
	/**
	 * @inherited
	 */
	public function parse($value)
	{
		$spl = explode(' ',$value);
		$module = array_shift($spl);
		$op = array_shift($spl);
		$version = array_shift($spl);
		$value = array(
			'module' => $module,
			'op' => $op,
			'version' => $version
		);

		return $value;
	}
	
} 