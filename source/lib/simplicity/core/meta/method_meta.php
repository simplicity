<?php
/**
 * smp_MethodMetaData
 *
 * Class extends the smp_ClassMetaData class to support parsing method doc comments.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_MethodMetaData extends smp_ClassMetaData {
	
	private $_method;
	
	/**
	 * @inherited
	 */
	protected function init($args) {
		$this->_class = $args[0];
		$this->_method = $args[1];
		
		return true;
	}

	/**
	 * @inherited
	 */
	public function getReflector() {
		return new ReflectionMethod($this->_class,$this->_method);
	}
}