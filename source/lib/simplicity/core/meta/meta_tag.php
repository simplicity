<?php
/**
 * smp_MetaTag
 *
 * Implemented by smp_MetaTag plugins, used to specify custom tags and how they should be parsed.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
abstract class smp_MetaTag
{
	protected $_multiple = true;
	
	/**
	 * parse
	 * 
	 * Should parse the given tag $value and return the parsed result.
	 * 
	 * @param $value string
	 * @return mixed
	 */
	abstract public function parse($value);
	
	/**
	 * hasMultipleValues
	 * 
	 * Returns true or false if a tag can be listed multiple times. The value of this should be set by overriding
	 * the protected member $_multiple.
	 *  
	 * @return boolean
	 */
	final public function hasMultipleValues() {
		return $this->_multiple;
	}
	
}