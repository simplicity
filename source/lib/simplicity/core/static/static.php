<?php
/**
 * Static Content Module
 *
 * Handles loading of static content, also loads files contained within compressed (smpa)
 * and un-compressed modules.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_StaticModule extends smp_Module
{

	private $_mime_types = array(
		'txt' => 'text/plain',
		'htm' => 'text/html',
		'html' => 'text/html',
		'php' => 'text/html',
		'css' => 'text/css',
		'js' => 'application/javascript',
		'json' => 'application/json',
		'xml' => 'application/xml',
		'swf' => 'application/x-shockwave-flash',
		'flv' => 'video/x-flv',

	// images
		'png' => 'image/png',
		'jpe' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'jpg' => 'image/jpeg',
		'gif' => 'image/gif',
		'bmp' => 'image/bmp',
		'ico' => 'image/vnd.microsoft.icon',
		'tiff' => 'image/tiff',
		'tif' => 'image/tiff',
		'svg' => 'image/svg+xml',
		'svgz' => 'image/svg+xml',

	// archives
		'zip' => 'application/zip',
		'rar' => 'application/x-rar-compressed',
		'exe' => 'application/x-msdownload',
		'msi' => 'application/x-msdownload',
		'cab' => 'application/vnd.ms-cab-compressed',

	//audio/video
		'mp3' => 'audio/mpeg',
		'qt' => 'video/quicktime',
		'mov' => 'video/quicktime',

	// adobe
		'pdf' => 'application/pdf',
		'psd' => 'image/vnd.adobe.photoshop',
		'ai' => 'application/postscript',
		'eps' => 'application/postscript',
		'ps' => 'application/postscript',

	//ms office
		'doc' => 'application/msword',
		'rtf' => 'application/rtf',
		'xls' => 'application/vnd.ms-excel',
		'ppt' => 'application/vnd.ms-powerpoint',

	//open office
		'odt' => 'application/vnd.oasis.opendocument.text',
		'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
	);

	/**
	 * @inherited
	 */
	protected function init()
	{
		$this->setDefault('cache_timeout',0);
	}

	/**
	 * exec
	 * 
	 * Parse the URI and return the appropriate static resource
	 *
	 * @param $uri
	 * @return mixed
	 */
	public function exec($args=array())
	{
		$uri = smp_Request::getInstance()->getUriSegments();
						
		if ($uri[0] == 'smp') {
			array_shift($uri);
			return $this->getSimplicityResource($uri);		
		}
		
		if (!$this->getAppResource($uri)) {
			$this->getModuleResource($uri);
		}
	}


	/**
	 * Find the resource for the given URI. Used to return top level app static resources.
	 *
	 * @param $uri
	 * @return void
	 */
	private function getSimplicityResource($uri)
	{
		$file = ROOT.'public'.DS.implode(DS,$uri);
		$this->sendResponse($file);
	}	
	
	/**
	 * Find the resource for the given URI. Used to return top level app static resources.
	 *
	 * @param $uri
	 * @return void
	 */
	private function getAppResource($uri)
	{
		$file = APP.'public'.DS.implode(DS,$uri);
		if (!file_exists($file)) return false;
		$this->sendResponse($file);
	}	
	
	/**
	 * Find the resource for the given URI. Used to return static resources contained in modules.
	 *
	 * @param $uri
	 * @return void
	 */
	private function getModuleResource($uri)
	{
		$alias = array_shift($uri);
				
		$alias = smp_String::camelize($alias,true);		
		
		$module = $this->getSimplicity()->getModuleAlias($alias);
		
		if (!$this->getSimplicity()->hasModule($module,'dynamic')) return false;

		$mod = $this->getSimplicity()->getModule($alias);
		
		if (!$mod) return false;
		
		$class = get_class($mod);
		
		if (substr($class,0,4) == 'smp_') {
			$mod = smp_String::underscore($mod->getAlias());
			$root = APP.'modules'.DS.$mod.DS.'public';
		}
		
		if (smp_Tapped::getInstance()->isArchive($class))
		{
			$path = 'public/'.implode('/',$uri);
			$root = smp_Tapped::getInstance()->getPath($class);
			$m = new smp_Archive($root);
			$data = $m->getFile($path);
			$etag = md5($data);

			return $this->sendData($data,$this->getMimeType($path),filemtime($path),$etag);
		}
		
		$file = $root.DS.implode(DS,$uri);
		
		return $this->sendResponse($file);
	}

	/**
	 * Return the given file sending appropriate headers on error.
	 *
	 * @param $file
	 * @return void
	 */
	private function sendResponse($file)
	{	
		if (!file_exists($file)) {
			smp_Response::getInstance()->setStatus(404);
			return $this->getSimplicity()->clearCurrentChain();
		}
		
		if (!is_readable($file)) {
			smp_Response::getInstance()->setStatus(403);
			return $this->getSimplicity()->clearCurrentChain();
		}
		
		$etag = md5_file($file);
		$last_mod = filemtime($file);

		return $this->sendData('file:'.$file,$this->getMimeType($file),$last_mod,$etag);
	}

	/**
	 * Send the given data in the response with the correct headers. Also handles sending and comparing
	 * Etag and Last-Modified headers.
	 *
	 * @param $data string
	 * @param $type string
	 * @param $last_mod int
	 * @param $etag string
	 * @return void
	 */
	private function sendData($data,$type,$last_mod,$etag)
	{
		smp_Response::getInstance()->setHeader('etag',$etag);
		smp_Response::getInstance()->setHeader('expires',gmdate('D, d M Y H:i:s \G\M\T',strtotime('+10 years')));
		smp_Response::getInstance()->setHeader('cache-control','must-revalidate');
		
		if (smp_Request::getInstance()->getHeader('if-none-match') == $etag)
		{
			smp_Response::getInstance()->setStatus(304);
			$this->getSimplicity()->clearCurrentChain();
			return true;
		}

		$lmod = smp_Request::getInstance()->getHeader('if-modified-since');
		if ($lmod && $last_mod >= $lmod)
		{
			smp_Response::getInstance()->setStatus(304);
			$this->getSimplicity()->clearCurrentChain();
			return true;
		}

		$last_mod = gmdate('D, d M Y H:i:s \G\M\T',$last_mod);

		if (substr($data,0,5) == 'file:')
		{
			$file = str_replace('file:','',$data);
			$length = filesize($file);
		} else {
			$length = strlen($data);
		}
		
		smp_Response::getInstance()->setHeader('content-length',$length);
		smp_Response::getInstance()->setHeader('content-type',$type);
		smp_Response::getInstance()->setHeader('last-modified',$last_mod);

		if (substr($data,0,5) == 'file:') $data = str_replace('file:','',$data);

		smp_Response::getInstance()->setContent(file_get_contents($data));		
		
		return true;
	}

	/**
	 * Return the mime type of the given file. Just uses the extension does nothing very intelligent.
	 * Maybe I will improve this one day.
	 *
	 * @param $file string
	 * @return string
	 */
	private function getMimeType($file) {
		$spl = explode('.',$file);
		$ext = strtolower(array_pop($spl));

		if (isset($this->_mime_types[$ext]))
		{
			return $this->_mime_types[$ext];
		}

		return 'application/octet-stream';
	}
}