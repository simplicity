<?php
/**
 * smp_FatalException
 *
 * Used to handle PHP fatal errors.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_FatalException extends smp_Exception {}