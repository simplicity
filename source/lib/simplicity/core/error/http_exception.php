<?php
/**
 * smp_HttpException
 *
 * Extends the base smp_Exception to track handle HTTP errors.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_HttpException extends smp_Exception
{
	static private $_status_codes = array(
		100 => 'Continue',
		101 => 'Switching Protocols',
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		307 => 'Temporary Redirect',
		400 => 'Bad Request',
		401 => 'Unathorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported'
	);	
	
	private $_heading = "HTTP Error";
	
	/**
	 * Constructor
	 * 
	 * Creates a new smp_HttpException, pass in the HTTP error code to create the exception.
	 * 
	 * @param $code int
	 */
	public function __construct($code,$message = null)
	{
		$code = isset(self::$_status_codes[$code]) ? $code : 500;
		$this->_heading = $code.' '.self::$_status_codes[$code];
		$msg = isset($message) ? $message : "";
		
		parent::__construct($msg,null,$code);
	}
	
	public function getHeading()
	{
		return $this->_heading;	
	}
}