<?php

abstract class smp_ErrorHandler {
	
	private $_error;
	private $_exception;
	private $_config;
	
	final public function __construct(smp_ErrorModule $error,smp_Exception $e,$alias)
	{
		$this->_error = $error;
		$this->_exception = $e;
		$alias = smp_String::underscore($alias);
		$this->_config = "modules.error.{$alias}";
			
		$this->init();
	}
	
	final protected function getConfig($path=null)
	{
		if (!isset($path)) $path = "";
		$path = "{$this->_config}.$path";
		return $this->getError()->getSimplicity()->getConfig($path);
	}
	
	/**
	 * @return smp_ErrorModule
	 */
	final protected function getError()
	{
		return $this->_error;
	}
	
	/**
	 * @return smp_Exception
	 */
	final public function getException()
	{
		return $this->_exception;
	}
	
	public function init() {}
		
	abstract public function handle();
}