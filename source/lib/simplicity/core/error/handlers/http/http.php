<?php
class smp_HttpErrorHandler extends smp_ErrorHandler 
{	
	public function handle()
	{	
		$e = $this->getException();
		
		if ($e instanceof smp_HttpException) {
			smp_Response::getInstance()->setStatus($e->getCode());
		}
		else {
			smp_Response::getInstance()->setStatus(500);	
		}
		
		
		$t->exception = $this->getException();
		
		smp_Response::getInstance()->setContent($t->renderTemplate());
		
	}	
}