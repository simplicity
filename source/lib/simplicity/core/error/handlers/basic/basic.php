<?php
class smp_BasicErrorHandler extends smp_ErrorHandler 
{	
	public function handle()
	{	
		$t = new smp_Template(dirname(__FILE__).DS.'default.phtml');
		$t->exception = $this->getException();
		
		smp_Response::getInstance()->setContent($t->renderTemplate());
		smp_Response::getInstance()->setStatus(500);
	}	
}