<?php
/**
 * Error Module
 *
 * The Simplicity Error module provides advanced error handling.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_ErrorModule extends smp_Module
{
	
	private $_handled = false;
		
	/**
	 * @inherited
	 */
	protected function init()
	{
		$this->setDefault('enabled',true);
		$this->setDefault('chain',array('Basic'));
		
		$this->createExtensionPoint('smp_ErrorHandler');
		$this->registerPluginPrefix('smp_');
	}

	/**
	 * @inherited
	 */	
	public function exec($args=array())
	{
		error_reporting(-1);
		
		if ($this->getConfig('enabled')) {
			ini_set('display_errors',0);
			register_shutdown_function(array($this,'shutdown'));
			set_error_handler(array($this,'error'));
			set_exception_handler(array($this,'exception'));
		}
	}

	/**
	 * @internal 
	 */
	public function error($errno,$errstr,$errfile,$errline)
	{
		restore_error_handler();
		$this->_handled = true;
		$this->handle(new smp_Exception($errstr,$errline,$errno,$errfile));
	}

	/**
	 * @internal 
	 */	
	public function shutdown()
	{
		restore_error_handler();
		restore_exception_handler();

		if ($this->_handled) return false;

		$e = error_get_last();
		
		if (!is_null($e)) {
			$e = new smp_FatalException('Fatal error: '.$e['message'],$e['line'],$e['type'],$e['file'],$GLOBALS,array());
			$this->handle($e);
		}
	}

	/**
	 * @internal 
	 */	
	public function exception(Exception $e)
	{
		restore_error_handler();
		restore_exception_handler();

		$this->_handled = true;

		if (!($e instanceof smp_Exception)) {
			$e = new smp_Exception($e->getMessage(),$e->getLine(),$e->getCode(),$e->getFile());
		}

		$this->handle($e);
	}

	public function getErrorReport($ref)
	{
		$ref = (int) $ref;
		
		$cnf = $this->getConfig('logging');
		$realpath = realpath($cnf['path']);
		
		if (!$realpath) return false;
		
		$realpath .= DS;
		
		$dsn = "sqlite:{$realpath}errorlog.sq3";
		
		$p = new PDO($dsn,null,null,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		$p->sqliteCreateFunction('crc32', 'crc32', 1);
		
		try {
			$qry = "create table if not exists error_log ( id integer primary key, timestamp default CURRENT_TIMESTAMP, message, trace, context, request, response )";
			$p->exec($qry);
			
			/*
			$qry = "select crc32(id || timestamp) as ref1,? as ref2,* from error_log";
			$stm = $p->prepare($qry);
			$stm->execute(array($ref));
			var_dump($stm->fetchAll(PDO::FETCH_ASSOC));die();
			*/
			
			$qry = "select * from error_log where crc32(id || timestamp) like ?";
			$stm = $p->prepare($qry);
			$stm->execute(array($ref));
			$error = $stm->fetch(PDO::FETCH_ASSOC);
			return $error;
		}
		catch (Exception $e) {
			var_dump($e);
			die();	
		}
		
		return false;
	}
	
	private function logException(smp_Exception $e)
	{	
		$msg = $e->getMessage() . ' on line ' . $e->getLine() . ' in file ' . $e->getFile();
		
		$session = Simplicity::getInstance()->getModule('Session');
		
		if ($last = $session->get('last_exception')) {
			if ($last['error']['message'] == $msg) return $last['ref'];	
		}
		
		$cnf = $this->getConfig('logging');
		
		$realpath = realpath($cnf['path']);
				
		if (!$realpath) return false;
		
		$realpath .= DS;
		
		$dsn = "sqlite:{$realpath}errorlog.sq3";
		$p = new PDO($dsn,null,null,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		
		try {
			$qry = "create table if not exists error_log ( id integer primary key, timestamp default CURRENT_TIMESTAMP, message, trace, context, request, response )";
			$p->exec($qry);
			
			$qry = "insert into error_log (message,trace,context,request,response) values (?,?,?,?,?)";
			$stm = $p->prepare($qry);
						
			$trace = $e->getTraceAsString();
			$context = print_r($e->getContext(),true);
			
			$request = print_r(array(
				'method' => smp_Request::getInstance()->getMethod(),
				'uri' => smp_Request::getInstance()->getUri(),
				'request_data' => smp_Request::getInstance()->getData(),
				'headers' => smp_Request::getInstance()->getHeaders()
			),true);
			
			$response = print_r(array(
				'status' => smp_Response::getInstance()->getStatus(),
				'encoding' => smp_Response::getInstance()->getContentEncoding(),
				'headers' => smp_Response::getInstance()->getHeaders(),
				'content' => smp_Response::getInstance()->getContent()
			),true);
			
			$stm->execute(array($msg,$trace,$context,$request,$response));
		}
		catch (Exception $e) {
			return false;	
		}
		
		$id = $p->lastInsertId();
		$error = $p->query("select * from error_log where id = {$id}")->fetch(PDO::FETCH_ASSOC);
		
		$ref = crc32($id.$error['timestamp']);
		
		$session->set('last_exception',array('ref'=>$ref,'error' => $error));
		
		$file = date('Ymdhis',strtotime($error['timestamp'])).'_'.$ref;
		$file = $realpath."{$file}.log";
		
		file_put_contents($file,print_r($error,true));
		
		return $ref;
	}
	
	private function handle(smp_Exception $e)
	{	
		$chain = $this->getConfig('chain');		
		
		while ($module = array_shift($chain)) {
			$cls = $this->getModuleClass($module); 
			$mod = new $cls($this,$e,$module);
			$mod->handle();
			$e = $mod->getException();
		}
		
		$this->getSimplicity()->clearCurrentChain();
	}
}