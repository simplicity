<?php
/**
 * smp_Exception
 *
 * Extends the base exception to track context information and request and response headers. 
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_Exception extends Exception
{
	private $_context = array();
	private $_headers = array();

	/**
	 * Constructor
	 * 
	 * Create a new exception providing the error $msg, $line number, error $code and $file. 
	 * 
	 * @param $msg string
	 * @param $line int
	 * @param $code int
	 * @param $file string
	 */
	public function __construct($msg,$line=null,$code=null,$file=null)
	{
		parent::__construct($msg);

		$this->file = $file;
		$this->line = $line;
		$this->code = $code;

		$this->processContext();
		$this->processHeaders();
	}

	/**
	 * getContext
	 * 
	 * Retrieve the context of this exception.
	 * 
	 * @return array
	 */
	public function getContext()
	{
		return $this->_context;
	}

	/**
	 * getHeaders
	 * 
	 * Retrieve the request and response headers of this exception.
	 * 
	 * @return array
	 */
	public function getHeaders()
	{
		return $this->_headers;
	}

	/**
	 * getSafeTrace
	 * 
	 * Return the backtrace with each argument run through print_r for easy display.
	 * 
	 * @return array
	 */
	public function getSafeTrace()
	{
		$trace = $this->getTrace();

		foreach ($trace as $i => &$item)
		{
			if (isset($item['args']) && is_array($item['args'])) {
				foreach ($item['args'] as &$arg) {
					$arg = print_r($arg,true);
				}
			}
		}
		return $trace;
	}

	private function processContext()
	{
		$this->_context = $GLOBALS;
		unset($this->_context['GLOBALS']);
		$this->_context['_ENV'] = $_ENV;
		ksort($this->_context);
	}

	private function processHeaders()
	{
		if (function_exists('apache_request_headers'))
		{
			$this->_headers['REQUEST'] = apache_request_headers(); 
		}
		if (function_exists('headers_list'))
		{
				
			$heads = headers_list();
			foreach ($heads as $head) {
				$head = explode(':',$head,2);
				$this->_headers['RESPONSE'][$head[0]] = trim($head[1]);
			}
		}

	}
}