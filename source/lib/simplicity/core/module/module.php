<?php

/**
 * smp_Module
 *
 * Provides the base class for all dyamic simplicity modules.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * @smp_core
 */
abstract class smp_Module extends smp_Pluggable {

	private $_simplicity;
	private $_alias;

	private $_next = array();

	private $_defaults = array();

	/**
	 * __construct
	 * 
	 * Instanciates a new module also calling the init() and postInit() hooks.
	 *
	 * @param Simplicity $s
	 * @param $alias
	 * @return unknown_type
	 */
	final public function __construct(Simplicity $s,$alias)
	{
		$this->_simplicity = $s;
		$this->_alias = $alias;

		$this->init();
		$this->postInit();
	}

	/**
	 * init
	 * 
	 * Called directly after the module is instanciated.
	 *
	 */
	protected function init() {
	}

	/**
	 * postInit
	 * 
	 * Called directly after init()
	 *
	 */
	protected function postInit() {
	}

	/**
	 * setDefault
	 * 
	 * Set a default configuration $value for the given config $path this module. This value is used whenever configuration is not available.
	 *
	 * @param $path string
	 * @param $value mixed
	 */
	protected function setDefault($path,$value)
	{
		$this->_defaults[$path] = $value;
	}

	/**
	 * getAlias
	 * 
	 * Return whatever the given alias for this module is.
	 *
	 * @return string
	 */
	final public function getAlias()
	{
		return $this->_alias;
	}

	/**
	 * getConfig
	 * 
	 * Get the config value for the given $path.
	 *
	 * @param $path string
	 * @return mixed
	 */
	final public function getConfig($path = null)
	{	
		if (!isset($path)) {
			$default = $this->_defaults;
			$path = 'modules.'.smp_String::underscore($this->_alias);
		}
		else {
			$default = isset($this->_defaults[$path]) ? $this->_defaults[$path] : null;
			$path = 'modules.'.smp_String::underscore($this->_alias).'.'.$path;
		}		
		
		$val = $this->getSimplicity()->getConfig()->get($path);
				
		if (isset($path)) {
			if (!isset($val)) {
				$val = isset($default) ? $default : null;
			}
		}
		else {
			$val = $val + $default;
		}
		
		return $val;
	}
	
	/**
	 * runNext
	 * 
	 * Tell Simplicity to execute the given module (or an array of modules) directly after this one completes.
	 *
	 * @param $module string|array
	 */
	final public function runNext($module)
	{
		$this->_next = is_array($module) ? array_merge($module,$this->_next) : array_merge(array($module),$this->_next);
	}

	/**
	 * getNext
	 * 
	 * Return (if set) any module configured to execute after this completes from a runNext() call.
	 *
	 * @return string|array
	 */
	final public function getNext()
	{
		return array_shift($this->_next);
	}

	/**
	 * getSimplicity
	 * 
	 * Returns the Simplicity instance.
	 *
	 * @return Simplicity
	 */
	final public function getSimplicity()
	{
		return $this->_simplicity;
	}

	/**
	 * exec
	 * 
	 * This method is called by default by Simplicity to execute the module. An array of arguments may be passed in as the only parameter.
	 *
	 * @param $args array
	 */
	abstract public function exec($args=array());
}
