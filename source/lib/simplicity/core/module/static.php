<?php


/**
 *  smp_ModuleStatic
 *
 *  Implemented by all static Simplicity modules.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * @smp_core
 */
interface smp_ModuleStatic {}