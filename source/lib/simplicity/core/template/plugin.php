<?php

/**
 * Template Plugin Interface
 * 
 * Developers making Template Plugins will extend this.    
 * 
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_core
 */
abstract class smp_TemplatePlugin {
	
	private $_template;
	
	final public function __construct(smp_Template $template)
	{
		$this->_template = $template;
		$this->init();
	}
	
	final protected function getTemplate()
	{
		return $this->_template;
	}
	
	protected function init() {}

}