<?php

/**
 * Template Module
 * 
 * The Simplicity Template module provides a simple PHP based templating system.    
 * 
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 * 
 * @smp_children smp_TemplatePlugin
 * @smp_core
 */
class smp_Template extends smp_Pluggable implements smp_ModuleStatic
{
	private $_data = array();
	private $_plugins = array();

	private $_parent;
	
	private $_template;
	private $_root;
	
	/**
	 * Constructor
	 * 
	 * Create a new smp_Template instance providing the $path to the template file. Do not specify the $parent at this time, it is used for 
	 * rendering sub templates defined by a call to render().
	 * 
	 * @param $path string
	 * @param $parent smp_Template 
	 */
	public function __construct($path=null,$parent=null)
	{	
		$this->createExtensionPoint('smp_TemplatePlugin');
		$this->registerPluginPrefix('smp_');
		
		if (!($parent instanceof smp_Template)) 
		{
			if (!file_exists($path)) {
				throw new Exception("{$path} does not exist. Failed to load template.");
			}		
		} else {
			if (!file_exists($path)) $path = $parent->getRoot().$path;
			if (!file_exists($path)) {
				return;
			}
		}
				
		$this->_template = $path;
		$this->_root = ($parent instanceof smp_Template) ? $parent->getRoot() : dirname($this->_template).DS;

		$this->_parent = $parent;
	}
	
	public function setParent(smp_Template $parent)
	{
		$this->_parent = $parent;
	}
	
	private function setRoot($path)
	{
		$this->_root = $path.DS;
	}
	
	/**
	 * getRoot
	 * 
	 * Get the root path of this template.
	 *  
	 * @return string
	 */
	public function getRoot()
	{
		return $this->_root;
	}
	
	public function getTemplate()
	{
		return $this->_template;
	}
	
	/**
	 * set
	 * 
	 * Set a value to be passed to the template. This will be accessible within
	 * the template as a property ($this->name). 
	 *
	 * @param $name string 
	 * @param $value mixed
	 */
	public function set($name,$value)
	{
		if ($value instanceof smp_Template) {
			$value->setParent($this);
		}
		$this->_data[$name] = $value;
	}
	
	/**
	 * get 
	 * 
	 * Get a value assigned to the template.
	 *
	 * @param string $name
	 * @return mixed
	 */
	public function get($name)
	{					
		$ret = null;
		
		if (!isset($this->_data[$name])) return $ret; 
		
		return $this->_data[$name];
	}

	private function &getRef($name)
	{
		if (($this->_parent instanceof smp_Template) && !$this->has($name)) {
			return $this->_parent->getRef($name);
		}		
		$ret = null;
		
		if (!isset($this->_data[$name])) return $ret; 
		 
		return $this->_data[$name];
	}
	
	private function &setRef($name,$value)
	{
		$this->_data[$name] = $value;
		return $this->_data[$name];
	}
	
 	/**
 	 * addItem
 	 * 
 	 * Add an items to the specified list, if the list does not exist a new list is created 
 	 * in the current template. You can also specify a key to make the list associative.
 	 * 
 	 * @param $list string
 	 * @param $value mixed
 	 * @param $key string
 	 * @return void
 	 */
 	public function addItem($list,$value,$key=null)
 	{	 		
 		$list_ref =& $this->getRef($list);
 		
 		if (!$list_ref) {
 			$list_ref =& $this->setRef($list,array());
 		}
 		
 		if (isset($key)) {
 			$list_ref[$key] = $value;
 		}
 		else {
 			$list_ref[] = $value;	
 		}
 	}
 	
	/**
	 * loadPlugin
	 * 
	 * Load a specific plugin and make it available to the template. 
	 * 
	 * You can either provide an instance or class name. By default, 
	 * once loaded a plugin will be available as a method of the template 
	 * by the same name ($this->MyPlugin()), alternatively you can 
	 * specify an identifier as the second param.   
	 *
	 * @param smp_TemplatePlugin $plugin
	 * @param string $identifier
	 * @return smp_TemplatePlugin
	 */
	public function loadPlugin($plugin,$identifier=null)
	{
		if ($plugin instanceof smp_TemplatePlugin) {
			$p = $plugin;
		}
		else 
		{
			if (!$plugin = $this->getModuleClass($plugin)) throw new Exception("Unknown template plugin {$plugin}");
			$p = new $plugin($this);
		}
		
		if (!isset($identifier)) {
			$identifier = $this->getClassModule(get_class($p));
		}
		
		$this->_plugins[$identifier] = $p;
		
		return $this->_plugins[$identifier];
	}
	
	/**
	 * getPlugin
	 * 
	 * Get an instance of a loaded plugin.
	 *
	 * @param string $name
	 * @return smp_TemplatePlugin
	 */
	public function getPlugin($name)
	{ 
		if (($this->_parent instanceof smp_Template) && !$this->hasPlugin($name)) {
			return $this->_parent->getPlugin($name);
		}
		
		if (isset($this->_plugins[$name]))
		{
			return $this->_plugins[$name];
		}
		
		return $this->loadPlugin($name);
	}
	
	/**
	 * hasPlugin
	 * 
	 * Return true of false in the given plugin $name exists.
	 * 
	 * @param $name string
	 * @return boolean
	 */
	public function hasPlugin($name)
	{
		return isset($this->_plugins[$name]);
	}
	
	/**
	 * @internal
	 */
	public function __call($name,$args)
	{
		return $this->getPlugin($name);
	}

	/**
	 * Allows access to template data via a overloaded property.
	 * 
	 * @internal
	 * @see get
	 */
	public function __get($name)
	{	
		return $this->get($name);
	}
	
	/**
	 * Allows you to set template data via a overloaded property.
	 * 
	 * @internal
	 * @see set
	 */
	public function __set($name,$value)
	{	
		return $this->set($name,$value);
	}
	
	/**
	 * has
	 * 
	 * Returns boolean true / false if the specified key is available.. 
	 *
	 * @param string $name
	 * @return boolean
	 */
	public function has($name)
	{
		return isset($this->_data[$name]);
	}
	
	/**
	 * __isset overloaded function.
	 *
	 * @internal
	 * @see has
	 * 
	 */
	public function __isset($name)
	{
		return $this->has($name);
	}
	
	/**
	 * render
	 * 
	 * Render the template at the given path and return a string. 
	 *
	 * @param string $path
	 * @return string
	 */
	public function render($path,$args=array())
	{	
		if (!$path) return false;

		if (!($path instanceof smp_Template)) {
			if (substr($path,0,2) == './') {
				$path = dirname($this->_template).DS.substr($path,2);
			}
			$tpl = new smp_Template($path,$this);		
		}
		else {
			$tpl = $path;
			$tpl->setParent($this);
		}
		
		foreach ($args as $k => $v) 
		{
			$tpl->set($k,$v);
		}
		
		return $tpl->renderTemplate();
	}
	
	/**
	 * renderTemplate
	 * 
	 * Render the root template and return the resulting string.
	 *
	 * @return string
	 */
	public function renderTemplate()
	{
		if (!($this->_parent instanceof smp_Template)) 
		{			
			foreach ($this->_data as $k => $val) {
				if ($val instanceof smp_Template) {
					$this->_data[$k] = $this->get($k);
				}
			}
		}
		
		ob_start(); 
		if (file_exists($this->_template)) 
		{
			if (substr($this->_template,0,7) == 'smpa://')
			{
				include smp_Archive::loadFileFromArchive($this->_template,true);
			}
			else 
			{
				include $this->_template;
			}
				
		}
		return ob_get_clean();	
	}
	
	public function transferData(smp_Template $target)
	{
		foreach ($this->_data as $key => $value) 
		{
			$target->set($key,$value);
			unset($this->_data[$key]);	
		}
	}
	
	public function e($str)
	{
		return htmlentities($str);
	}
}