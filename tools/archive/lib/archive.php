<?php

/**
 * smp_Archive
 * 
 * The smp_Archive class allows you to create and access smpa format archives. Similar to Phar, but was designed specifically for simplifying
 * the distribution and installation of Simplicity modules.
 *
 * @author John Le Drew <jp@antz29.com>
 * @copyright Copyright (c) 2009, John Le Drew
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 * @version 2.0.0-alpha
 *
 * @smp_core
 */
class smp_Archive {

	private $_file;

	private $_new = false;
	private $_data = array();
	private $_classes = array();
	
	static private $_tmp = null;	
	static private $_archives = array();

	/**
	 * getThisArchive
	 * 
	 * When called from a file contained in an archive it will return the smp_Archive object for the file. Can also be called
	 * passing in a path to an smp archive to load.
	 *
	 * @param $path string
	 * @return smp_Archive
	 */
	static public function getThisArchive($path=null)
	{
		if (!isset($path)) {
			$trace = debug_backtrace();
			$path = $trace[0]['file'];
		}

		$spl = array_merge(array_filter(explode(DS,$path)));

		while ($seg = array_shift($spl))
		{
			if (stristr($seg,'.smpa'))
			{
				$mod = array_merge(array_filter(explode('_',$seg)));
				$mod = array_shift($mod);

				return isset(self::$_archives[$mod]) ? new smp_Archive(self::$_archives[$mod]) : false;
			}
		}

		return false;
	}

	/**
	 * loadFileFromArchive
	 * 
	 * Include the php file specified in the url. Should be in the format smpa://archive.smpa/path/in/archive.php. You can optionally only
	 * request the path to the temporary file used to access the file.
	 *
	 * @param $url string
	 * @param $get_path_only boolean
	 * @return boolean|string
	 */
	static public function loadFileFromArchive($url,$get_path_only=false)
	{
		$url = self::parseUrl($url);
		$a = new smp_Archive($url['archive']);
		return $a->loadFile($url['file'],$get_path_only);
	}

	/**
	 * getFileFromArchive
	 * 
	 * Load the contents of a file from within an archive by url in the format smpa://archive.smpa/file/in/archive
	 *
	 * @param $url string
	 * @return string
	 */
	static public function getFileFromArchive($url)
	{
		$url = self::parseUrl($url);
		$a = new smp_Archive($url['archive']);
		return $a->getFile($url['file']);
	}

	/**
	 * init
	 * 
	 * Initialize the smpa stream handler
	 *
	 */
	static public function init()
	{
		stream_wrapper_register('smpa', __CLASS__,0);
	}

	/**
	 * getPath
	 * 
	 * Return the full path the the archive.
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->_file;
	}

	/**
	 * Constructor
	 * 
	 * Instanciates a new smp_Archive object opening the archive at the given path. If you want to create a new archive pass boolean true as the
	 * second parameter.
	 *
	 * @param $file string
	 * @param $create boolean
	 */
	public function __construct($file,$create=false)
	{
		$exists = file_exists($file);
		$this->_file = $exists ? realpath($file) : $file;

		if ($create) {
			if (!is_writable(dirname($file))) throw new Exception("Cannot create archive at specified location, permission denied.");
			touch($file);
			$this->_new = true;
		} elseif(!$exists) {
			throw new Exception("Cannot file {$file} not found.");
		}

		if (!$create) $this->load();
	}

	/**
	 * addDir
	 * 
	 * Recursively add an entire directory to the archive. If you specify boolean true for the second parameter the directory contents is
	 * added at the root level of the archive.
	 *
	 * Adding the directory foo that contains a file called bar without the second parameter would leave you with an archive with the
	 * following structure: /foo/bar
	 *
	 * Adding the directory with the second parameter would leave you with: /bar
	 *
	 * @param $path string
	 * @param $root boolean
	 */
	public function addDir($path,$root=false)
	{
		if (!$this->_new) return false;
		if (!$path = realpath($path)) throw new Exception("Cannot add directory. '{$path}' is not accessable.");

		$it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));

		foreach ($it as $file)
		{
			$target = $file->getPathName();
				
			if ($root)
			{
				$target = str_replace(dirname($path).DS.basename($path),'',$target);
			} else {
				$target = str_replace(dirname($path),'',$target);
			}
				
			$this->addFile($file->getPathName(),$target);
		}
	}

	/**
	 * addFile
	 * 
	 * Add the specified $file to the archive, optionally specifying its new $target path in the archive.
	 *
	 * @param $file string
	 * @param $target string
	 */
	public function addFile($file,$target=null)
	{
		if (!$this->_new) return false;

		if ($classes = $this->parseFile((string) $file))
		{
			foreach ($classes as $type => $cls)
			{
				foreach ($cls as $class) {
					if (!isset($this->_classes[$class]))
					{
						$this->_classes[$class]['type'] = $type;
						$this->_classes[$class]['file'] = $target;
					}
				}
			}
		}
		$spl = array_merge(array_filter(explode(DS,$target)));
		$file_name = array_pop($spl);
		$lev =& $this->_data;
		while ($path = array_shift($spl))
		{
			if (!isset($lev[$path])) $lev[$path] = array();
			$lev =& $lev[$path];
		}

		$lev[$file_name]['stat'] 	= stat($file);
		$lev[$file_name]['md5'] 	= md5_file($file);
		$lev[$file_name]['data'] 	= gzcompress(file_get_contents($file),5);
	}

	/**
	 * parseFile
	 * 
	 * Parse the given $file for all contained classes and interfaces, and return this as an array.
	 *
	 * @param $file
	 * @return unknown_type
	 */
	private function parseFile($file)
	{
		if (substr(basename($file),-3) != 'php') return false;

		$tokens = token_get_all(file_get_contents($file));
		$classes = array();
		foreach ($tokens as $i => $token)
		{
			if ($token[0] == T_CLASS || $token[0] == T_INTERFACE)
			{
				$i += 2;
				$type = ($token[0] == T_CLASS) ? 'class' : 'interface';
				$classes[$type][$tokens[$i][1]] = $tokens[$i][1];
			}
		}

		return $classes;
	}

	/**
	 * getClasses
	 * 
	 * Returns an array of every class or interface contained in the archive.
	 *
	 * @return array
	 */
	public function getClasses()
	{
		return array_keys($this->_classes);
	}

	/**
	 * loadClass
	 * 
	 * Load the specified $class from the archive.
	 *
	 * @param $class string
	 */
	public function loadClass($class)
	{
		if (!$file = $this->getClassFile($class))
		{
			throw new Exception("Failed loading class from archive, class {$class} not found.");
		}

		return $this->loadFile($file['file']);
	}

	/**
	 * loadFile
	 * 
	 * Load the specified $file from the archive, optionally (by passing boolean true as the second parameter) only returning the temporary
	 * path of the file loaded.
	 *
	 * @param $file string
	 * @param $get_path_only boolean
	 * @return unknown_type
	 */
	public function loadFile($file,$get_path_only=false)
	{
		$modhash = md5($this->_file);

		$tmp = defined('TMP') ? TMP : sys_get_temp_dir().DS;
		$tmproot = $tmp.$modhash.'_'.basename($this->_file);

		$filename = str_replace('/','_',$file);

		$path = $tmproot.DS.$filename;

		if (!file_exists($path))
		{
				
			$data = $this->getFile($file);
				
			if (!file_exists($tmproot) && !mkdir($tmproot))
			{
				throw new Exception("Failed loading ffile from archive, cannot create tmp dir {$tmproot}.");
			}

			if (!file_put_contents($path,$data))
			{
				throw new Exception("Failed loading file from archive, cannot write to tmp file {$path}.");
			}
		}

		self::$_archives[$modhash] = $this->_file;

		if ($get_path_only) return $path;

		return include $path;
	}

	/**
	 * getClassFile
	 * 
	 * Return the file from the archive that contains the given $class.
	 *
	 * @param $class string
	 * @return string
	 */
	private function getClassFile($class)
	{
		return isset($this->_classes[$class]) ? $this->_classes[$class] : false;
	}

	/**
	 * hasClass
	 * 
	 * Returns boolean true or false if the given $class is contained in the archive.
	 *
	 * @param $class string
	 * @return boolean
	 */
	public function hasClass($class)
	{
		return isset($this->_classes[$class]);
	}

	/**
	 * hasFile
	 * 
	 * Returns boolean true or false if the given $file is contained in the archive.
	 *
	 * @param $file string
	 * @return boolean
	 */
	public function hasFile($file)
	{
		$spl = array_merge(array_filter(explode(DS,$file)));
		$lev =& $this->_data;
		while ($seg = array_shift($spl)) {
			if (!isset($lev[$seg])) return false;
			$lev =& $lev[$seg];
		}
		return true;
	}

	/**
	 * isDir
	 * 
	 * Returns boolean true or false if the given $path is a directory.
	 *
	 * @param $path string
	 * @return boolean
	 */
	public function isDir($path)
	{
		$spl = array_merge(array_filter(explode(DS,$path)));

		$lev =& $this->_data;
		while ($seg = array_shift($spl)) {
			if (!isset($lev[$seg])) return false;
			$lev =& $lev[$seg];
		}

		if (is_array($lev) && !isset($lev['md5']) && !isset($lev['stat']) && !isset($lev['data'])) {
			return true;
		}

		return false;
	}

	/**
	 * getStat
	 * 
	 * Returns the stat for the given $file in the archive.
	 *
	 * @param $file string
	 * @return array
	 */
	public function getStat($file)
	{
		$spl = array_merge(array_filter(explode(DS,$file)));

		$lev =& $this->_data;
		while ($seg = array_shift($spl)) {
			if (!isset($lev[$seg])) throw new Exception("Error getting file data, path {$file} not found.");
			$lev =& $lev[$seg];
		}

		if (!isset($lev['md5']) || !isset($lev['data']) || !isset($lev['stat'])) {
			throw new Exception("Error getting file data, unexpected archive format.");
		}

		return $lev['stat'];
	}

	/**
	 * getFile
	 * 
	 * Return the contents of the specified $file in the archive.
	 *
	 * @param $file string
	 * @return boolean
	 */
	public function getFile($file)
	{
		$spl = array_merge(array_filter(explode(DS,$file)));

		$lev =& $this->_data;
		while ($seg = array_shift($spl)) {
			if (!isset($lev[$seg])) throw new Exception("Error getting file data, path {$file} not found.");
			$lev =& $lev[$seg];
		}

		if (!isset($lev['md5']) || !isset($lev['data']) || !isset($lev['stat'])) {
			throw new Exception("Error getting file data, unexpected archive format.");
		}

		$data = gzuncompress($lev['data']);

		if (md5($data) != $lev['md5']) {
			throw new Exception("Error getting file, md5 sum mismatch.");
		}

		return $data;
	}

	/**
	 * load
	 * 
	 * Load the archive.
	 *
	 */
	private function load()
	{
		list($this->_data,$this->_classes) = include($this->_file);
	}

	/**
	 * save
	 * 
	 * Save the data to the archive.
	 *
	 */
	public function save()
	{
		if (!$this->_new) return false;
		$data = array($this->_data,$this->_classes);
		$data = '<?php return '.var_export($data,true).';';
		return file_put_contents($this->_file,$data);
	}

	/**
	 * parseUrl
	 * 
	 * Parse the given url and return the given archive and path it refers to. Should be in the format smpa://archive.smpa/path/to/file
	 * Returns an associative array in the format: array('archive'=>'path','file'=>'path');
	 *
	 * @param $path string
	 * @return array
	 */
	static public function parseUrl($path)
	{
		$url = array_merge(array_filter(explode(DS,$path)));
		array_shift($url);
		$path = "";
		while ($seg = array_shift($url))
		{
			$path .= DS.$seg;
			if (stristr($seg,'.smpa'))
			{
				break;
			}
		}

		$file = DS.implode(DS,$url);

		return array('archive'=>$path,'file'=>$file);
	}

	/**
	 * url_stat
	 * 
	 * Return the stat for the file referred to by $path.
	 *
	 * @param $path string
	 * @return array
	 */
	public function url_stat($path,$flags)
	{
		$path = self::parseUrl($path);

		$file = $path['file'];
		$path = $path['archive'];

		$a = new smp_Archive($path);

		if (!$a->hasFile($file)) throw new Exception("File, {$file} not found in archive {$path}.");

		if (!$a->isDir($file))
		{
			$mode = 0100666;
			$stat = $a->getStat($file);
			$size = $stat['size'];
		} else {
			$mode = 040777;
			$size = 4096;
		}

		return array (
		0 => 0,
			'dev' => 0,
		1 => 0,
			'ino' => 0,
		2 => $mode,
			'mode' => $mode,
		3 => 0,
			'nlink' => 0,
		4 => 0,
			'uid' => 0,
		5 => 0,
			'gid' => 0,
		6 => 0,
			'rdev' => 0,
		7 => $size,
			'size' => $size,
		8 => fileatime($path),
			'atime' => fileatime($path),
		9 => filemtime($path),
			'mtime' => filemtime($path),
		10 => filectime($path),
			'ctime' => filectime($path),
		11 => 0,
			'blksize' => 0,
		12 => 0,
			'blocks' => 0
		);
	}
}