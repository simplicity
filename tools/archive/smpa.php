#!/usr/bin/php

<?php
/**
 * An alias of the php constant DIRECTORY_SEPARATOR 
 */
define('DS',DIRECTORY_SEPARATOR);

/**
 * The path the this application.
 */
define('ROOT',realpath(dirname(__FILE__)).DS);

/**
 * This is the folder which stores files created during the sessions.
 */
define('TMP_ROOT',realpath(sys_get_temp_dir()).DS.'.smpa'.DS);

$path = ROOT.'lib'.DS.'tapped.php';
require $path;

$t = Tapped::getInstance()->setCache(0)->addPath(ROOT);
$t->registerAutoloader();

$a = new Smpa();

//strip out the first command line argument (contains the file name)
array_shift($argv);

//run transition
$a->run($argv);
