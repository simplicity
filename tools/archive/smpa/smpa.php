<?php

class Smpa extends Rebound {
	
	protected function init() {}
	
	protected function preAction($action,$args)
	{
		// If required attempt to create the TMP_ROOT root path.
		if (!file_exists(TMP_ROOT)) {
			if (!@mkdir(TMP_ROOT)) {
				$this->error('Failed to create temp folder at '.TMP_ROOT);
			}
		}
	}
	
	public function action_create($source,$target)
	{
		var_dump($source,$target);
		$source = realpath($source);
		$target = $target;
		
		// Create a unique session root path for this session
		$root = TMP_ROOT.md5($source).DS;

		// Create the session root folder
		if (!file_exists($root) && !mkdir($root)) {
			$this->error('Failed to create session root folder');
		}
		
		echo "Creating empty archive ({$target})...\n";
		$a = new smp_Archive($target,true);
		
		echo "Adding files ({$source})...\n";
		$a->addDir($source,true);
		
		echo "Saving data...\n";
		$a->save();		
	}
}
